#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <fstream>
#include "Trie.h"
#include "File.h"

void menu(fileData &file, Trie &trie)
{
   int n;
   do
   {
      std::cout << "Seleccione una opcion:\n1. Comprimir archivo\n2. Descromprimir archivo\n3. Salir\n";
      std::cin >> n;
   } while (n < 0 || n > 3);

   switch (n)
   {
   case 1:
      std::cout << "Comprimiendo archivo...\n";
      file.comprimirArchivo();
      trie.printearArbol(trie.root);
      menu(file, trie);
      break;
   case 2:
      std::cout << "Descromprimiendo archivo...\n";
      file.descomprimirArchivo("compresion.txt", "test-desc.txt", trie);
      break;
   default:
      break;
   }
}

int main()
{
   Trie trie;
   std::cout << "Ingrese el nombre del archivo y extension a comprimir: ";
   std::string nombreArchivo;
   std::cin >> nombreArchivo;
   fileData file(nombreArchivo, trie);
   menu(file, trie);
   return 0;
}
