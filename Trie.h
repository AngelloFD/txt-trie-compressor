#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

// Nodo del arbol de prefijos
/**
 * @brief Estructura TrieNode que representa un nodo del arbol de prefijos
 */
struct TrieNode
{
   bool esPalabra;                                // indica si el recorrido hasta este nodo es una palabra
   std::vector<long> nLinea;                      // indica el numero de linea respecto al archivo de texto
   std::vector<long> nPalabra;                    // indica el numero de palabra
   std::unordered_map<char, TrieNode *> children; // mapa para almacenar los nodos hijos

   /**
    * @brief Constructor de la estructura TrieNode. Inicializa sin ser palabra y con numero de linea 0 y numero de palabra 0.
    */
   TrieNode() : esPalabra(false), nLinea({0}), nPalabra({0}) {}
};

/**
 * @brief Estructura Trie que representa el arbol de prefijos
 */
struct Trie
{
public:
   TrieNode *root; // nodo raiz del Trie
   // Constructor
   Trie()
   {
      root = new TrieNode(); // Inicializa el nodo raiz
   }

   /**
    * @brief Funcion que inserta un nodo compuesto por la palabra, su numero de linea y su numero de palabra en el arbol Trie
    * @param palabra La palabra a insertar
    * @param nLinea El numero de linea respecto al archivo de texto
    * @param nPalabra El numero de palabra
    */
   void insert(const std::string &palabra, long nLinea, long nPalabra)
   {
      TrieNode *node = root;
      for (char c : palabra)
      {
         //  Si el caracter no esta presente entre los hijos
         if (node->children.find(c) == node->children.end())
         {
            node->children[c] = new TrieNode(); // Crear un nuevo nodo hijo
         }
         node = node->children[c]; // Mover el nodo actual al nodo hijo correspondiente
      }
      node->esPalabra = true; // Marcar el último nodo como una palabra
      if (node->nLinea[0] == 0 && node->nPalabra[0] == 0)
      {
         node->nLinea[0] = nLinea;     // Actualizar el numero de linea
         node->nPalabra[0] = nPalabra; // Actualizar el numero de palabra
      }
      else
      {
         node->nLinea.push_back(nLinea);     // Añadir el numero de linea respecto al documento
         node->nPalabra.push_back(nPalabra); // Añadir el numero de palabra
      }
   }

   std::string buscarPalabra(const std::string &palabra, long nLinea, long nPalabra)
   {
      TrieNode *node = root;
      for (char c : palabra)
      {
         if (node->children.find(c) == node->children.end())
         {
            return "NaN";
         }
         node = node->children[c];
      }
      if (node != nullptr && node->esPalabra)
      {
         // res tiene que ser la palabra
         std::string res = palabra;
         for (int i = 0; i < node->nLinea.size(); i++)
         {
            if (node->nLinea[i] == nLinea && node->nPalabra[i] == nPalabra)
            {
               return res;
            }
         }
      }
      return "NaN";
   }

   /**
    * @brief Funcion que busca una visualizar el arbol Trie en consola
    * @param node Nodo actual a visualizar
    * @param espacio Espacio de separacion entre nodos
    */
   void printearArbol(TrieNode *node, int espacio = 2)
   {
      if (node == nullptr)
         return;

      for (auto &child : node->children)
      {
         std::cout << std::string(espacio, ' ') << child.first << '\n';
         printearArbol(child.second, espacio + 2);
      }
      std::flush(std::cout); // limpiar el buffer de salida
   }
};