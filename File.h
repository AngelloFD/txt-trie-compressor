#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

struct fileData
{
private:
   std::string nombreArchivo;
   Trie trie;

public:
   fileData(const std::string &nombreArchivo, Trie trie) : nombreArchivo(nombreArchivo), trie(trie) {}

   /**
    * @brief Funcion que lee un archivo de texto por lineas y los inserta en un Trie junto con su numero de linea y numero de palabra
    */
   void comprimirArchivo()
   {
      std::ifstream archivo(nombreArchivo);
      if (!archivo.is_open())
      {
         std::cerr << "Error al abrir el archivo " << nombreArchivo << '\n';
         return;
      }
      long linea = 1;
      long numPalabra = 1;
      std::string strLinea;
      while (std::getline(archivo, strLinea)) // Mientras que se este leyendo el archivo linea por linea
      {
         std::istringstream iss(strLinea);
         std::string palabra;
         while (iss >> palabra) // Mientras que se este leyendo la linea palabra por palabra
         {
            trie.insert(palabra, linea, numPalabra); // Insertar la palabra en el Trie
            numPalabra++;                            // Incrementar el numero de palabra
         }
         linea++; // Incrementar el numero de linea
      }
      archivo.close();

      // Guardar compresion en archivo
      std::ofstream archivoSalida("compresion.txt");
      if (!archivoSalida.is_open())
      {
         std::cerr << "Error al abrir el archivo de salida compresion.txt\n";
         return;
      }
      // Llama a la función auxiliar para guardar el nodo raíz
      guardarNodo(archivoSalida, trie.root, "", 1, 1);
      archivoSalida.close();
   }

   void guardarNodo(std::ofstream &archivo, TrieNode *node, const std::string &prefix, int nLinea, int nPalabra)
   {
      if (node == nullptr)
      {
         return;
      }

      // Guardar nodo actual solo si es palabra
      if (node->esPalabra)
      {
         archivo << prefix;
         for (int i = 0; i < node->nLinea.size(); i++)
         {
            if (node->esPalabra == true)
            {
               archivo << " L" << node->nLinea.at(i) << " P" << node->nPalabra.at(i);
            }
         }
         archivo << '\n';
      }

      for (auto &child : node->children)
      {
         guardarNodo(archivo, child.second, prefix + child.first, nLinea, nPalabra);
      }
   };

   void descomprimirArchivo(const std::string &nombreArchivo, const std::string &nombreArchivoSalida, Trie &trie)
   {
      long linea = 1;
      long numPalabra = 1;
      // Buscar la palabra en el Trie de acuerdo a la linea y numero de palabra que deben coincidir en el nodo
      std::ifstream archivo(nombreArchivo);
      if (!archivo.is_open())
      {
         std::cerr << "Error al abrir el archivo " << nombreArchivo << '\n';
         return;
      }
      std::ofstream archivoSalida(nombreArchivoSalida, std::ios_base::app);
      if (!archivoSalida.is_open())
      {
         std::cerr << "Error al abrir el archivo de salida " << nombreArchivoSalida << '\n';
         return;
      }
      std::string strLinea;
      while (std::getline(archivo, strLinea)) // Mientras que se este leyendo el archivo linea por linea
      {
         std::istringstream iss(strLinea);
         std::string palabra;

         while (iss >> palabra) // Mientras que se este leyendo la linea palabra por palabra
         {
            // Buscar palabra en el Trie, obtener la linea y numero de palabra y escribir en el archivo de salida
            std::string res = trie.buscarPalabra(palabra, linea, numPalabra);
            archivoSalida << res << '\n';
            numPalabra++; // Incrementar el numero de palabra
         }
         linea++; // Incrementar el numero de linea
      }
      archivo.close();
      archivoSalida.close();
   }
};